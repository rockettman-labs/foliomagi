import React from 'react'

import Link from 'src/components/Link'
import { useAuthenticator } from '@aws-amplify/ui-react'

import Fab from '@mui/material/Fab'
import AddIcon from '@mui/icons-material/Add';
import { deckUrl, newDeck } from 'src/utils';
import { DataStore } from '@aws-amplify/datastore';
import { useRouter } from 'next/router';

export default function NewDeckFab() {

  const { user } = useAuthenticator(context => [context.user]);
  const router = useRouter();

  async function initNewDeck() {
    const deck = await DataStore.save(newDeck())
    router.push(deckUrl(user.username, deck.id))
  }

  return (
    <Fab
      onClick={initNewDeck}
      color='primary'
      aria-label='add'
      sx={{
        position: 'absolute',
        bottom: (theme) => theme.spacing(2),
        right: (theme) => theme.spacing(2)
      }}
    >
      <AddIcon />
    </Fab>
  )
}
