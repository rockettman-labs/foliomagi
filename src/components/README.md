# FolioMagi components

Component organization is based on [Atomic Design](https://bradfrost.com/blog/post/atomic-web-design/) principles but with different, more semantic folder names:

- ```Atoms``` -> Mostly from [Material UI]() but any custom atoms are in the root **components** folder
- ```Molecules``` -> in the root **components** folder as well
- ```Organisms``` -> in the **components/views** folder
- ```Templates``` -> in the **components/layouts** folder
- ```Pages``` -> in the **src/pages** folder as required by NextJS