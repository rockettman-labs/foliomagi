import React from 'react'

import Drawer from '@mui/material/Drawer'


export default function TemporaryDrawer(props: {
  anchor: 'right' | 'left',
  open: boolean,
  drawerContents: React.ReactNode,
  onClose?: ((event: {}, reason: "backdropClick" | "escapeKeyDown") => void) | undefined,
}) {
  const {anchor, open, drawerContents, onClose} = props

  return (
    <Drawer
      anchor={anchor}
      open={open}
      onClose={onClose}
      PaperProps={{
        sx: {
          width: {
            xs: '90%',
            sm: '50%',
            md: '40%',
            lg: '30%',
            xl: '20%'
          },
          overflowY: 'hidden'
        }
      }}
    >
      {drawerContents}
    </Drawer>
  )
}