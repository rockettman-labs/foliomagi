import Image from 'next/image'

export default function CustomIcon(src: string, label: string) {
  return (
    <Image src={src} alt={`${label} icon`} width={20} height={20} />
  )
}