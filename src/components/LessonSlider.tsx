import React from 'react'

import { LessonType, LessonTypeIcons } from 'src/cards'

import { styled } from '@mui/material/styles'
import Slider, { SliderProps } from '@mui/material/Slider'
import Stack from '@mui/material/Stack'
import Typography from '@mui/material/Typography'

interface StyledSliderProps extends SliderProps {
  lesson: LessonType
}

const StyledSlider = styled(Slider, {
  shouldForwardProp: (prop) => prop !== 'lesson'
})<StyledSliderProps>(({lesson}) => ({
  '& .MuiSlider-thumb': {
    background: `url(${LessonTypeIcons[lesson].filled})`
  }
}))

export default function LessonSlider(props: {
  lessonType: LessonType,
  currentValue: number,
  onChange: (newValue: number) => void
}) {
  const {lessonType, currentValue, onChange} = props

  const [value, setValue] = React.useState(0)

  React.useEffect(() => setValue(currentValue), [currentValue])

  const handleChange = (e: any, v: number | number[]) => {
    if (!Array.isArray(v)) {
      setValue(v)
    }
  }

  const handleCommit = (e: any, v: number | number[]) => {
    if (!Array.isArray(v)) {
      onChange(v)
    }
  }

  return (
    <Stack
      alignItems='center'
      spacing={2}
    >
      <StyledSlider
        lesson={lessonType}
        aria-label={`lesson-slider-${lessonType}`}
        orientation='vertical'
        min={0}
        max={30}
        value={value}
        valueLabelDisplay='off'
        onChange={handleChange}
        onChangeCommitted={handleCommit}
        sx={{ flex: 1 }}
      />
      <Typography color={value == 0 ? '#ffffff70' : undefined}>
        {value}
      </Typography>
    </Stack>
  )
}