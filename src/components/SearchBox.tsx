import React from 'react';

import { CardCatalog } from 'src/cards';

import { alpha, styled } from '@mui/material/styles';
import InputBase from '@mui/material/InputBase';
import SearchIcon from '@mui/icons-material/Search';


const Search = styled('div')(({ theme }) => ({
  position: 'relative',
  borderRadius: theme.shape.borderRadius,
  backgroundColor: alpha(theme.palette.common.white, 0.15),
  '&:hover': {
    backgroundColor: alpha(theme.palette.common.white, 0.25),
  },
  flexGrow: 1,
}));

const SearchIconWrapper = styled('div')(({ theme }) => ({
  padding: theme.spacing(0, 2),
  height: '100%',
  position: 'absolute',
  pointerEvents: 'none',
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
}));

const StyledInputBase = styled(InputBase)(({ theme }) => ({
  color: 'inherit',
  '& .MuiInputBase-input': {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)})`,
    transition: theme.transitions.create('width'),
    flexGrow: 1
  },
}));

export default function SearchBox(props: {
  placeholder: string,
  catalog?: CardCatalog,
  resultsCallback: (results: any) => void
}) {
  const {placeholder, catalog, resultsCallback} = props

  const [searchQuery, setSearchQuery] = React.useState('')

  const searchCatalog = (query: string) => {
    if (catalog !== undefined) {
      catalog.search(query)
        .then(searchResults => {
          resultsCallback(searchResults)
        })
        .catch(e => {
          console.error('[ERROR] Failed to search card catalog')
          console.error(JSON.stringify(e, null, 2))
        })
    }
  }

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSearchQuery(event.target.value)
    searchCatalog(searchQuery)
  };

  const handleKeyPress = (event: React.KeyboardEvent<HTMLDivElement>) => {
    if (event.key === 'Enter') {
      searchCatalog(searchQuery)
    }
  }

  return (
    <Search>
      <SearchIconWrapper>
        <SearchIcon />
      </SearchIconWrapper>
      <StyledInputBase
        placeholder={placeholder}
        inputProps={{ 'aria-label': 'search' }}
        value={searchQuery}
        onChange={handleChange}
        onKeyPress={handleKeyPress}
      />
    </Search>
  )
}