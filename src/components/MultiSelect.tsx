import React from 'react'

import CustomIcon from 'src/components/CustomIcon';

import Checkbox from '@mui/material/Checkbox';
import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import Select, { SelectProps } from '@mui/material/Select';
import OutlinedInput from '@mui/material/OutlinedInput';
import Box from '@mui/material/Box';
import Chip from '@mui/material/Chip';
import MenuItem from '@mui/material/MenuItem';
import ListItemText from '@mui/material/ListItemText';
import BlockIcon from '@mui/icons-material/Block';
import IconButton from '@mui/material/IconButton';

type MultiSelectProps<T> = Partial<SelectProps<T[]>> & {
  label: string;
  options: T[];
  disabledOptions?: T[];
  value: T[];
  negated: T[];
  onNegate: (value: T) => void;
  chipOnDelete: (value: T) => (event: any) => void;
  getIconPaths?: (value: T) => {filled: string, outlined: string};
}

export default function MultiSelect<T extends React.ReactNode>(props: MultiSelectProps<T>) {
  const {
    label,
    options,
    disabledOptions = [],
    value,
    negated,
    onNegate,
    chipOnDelete,
    getIconPaths,
    ...other
  } = props

  const useCustomIcon = getIconPaths !== undefined

  return (
    <FormControl
      fullWidth
      sx={{
        marginTop: (theme) => theme.spacing(1),
        marginBottom: (theme) => theme.spacing(3)
      }}
    >
      <InputLabel id="multi-select-label">{label}</InputLabel>
      <Select
        {...other}
        labelId="multi-select-label"
        id={`multi-select-${label}`}
        multiple
        value={value}
        input={<OutlinedInput label={label} />}
        renderValue={(selected) => (
          <Box sx={{ display: 'flex', flexWrap: 'wrap', gap: 0.75 }}>
            {selected.map(value => {
              const disabled = disabledOptions.includes(value)
              return (
                <Chip
                  key={`${value}`}
                  label={`${value}`}
                  onDelete={!disabled ? chipOnDelete(value) : undefined}
                  disabled={disabled}
                  onMouseDown={(event) => {
                    event.stopPropagation();
                  }}
                  {...(
                    negated.includes(value) ?
                    {style: {backgroundColor: 'red'}}
                    : {}
                  )}
                />
              )
            })}
          </Box>
        )}
      >
        {options.map(option => (
          <MenuItem
            key={`${option}`}
            value={`${option}`}
            disabled={disabledOptions.includes(option)}
          >
            <Checkbox
              checked={value.includes(option) && !negated.includes(option)}
              edge='start'
              disableRipple
              icon={useCustomIcon ?
                CustomIcon(getIconPaths(option).outlined, `${option}`)
                : undefined
              }
              checkedIcon={useCustomIcon ?
                CustomIcon(getIconPaths(option).filled, `selected ${option}`)
                : undefined
              }
            />
            <ListItemText primary={option}
              sx={{
                marginLeft: (theme) => theme.spacing(3)
              }}
            />
            <IconButton onClick={(event) => {
              event.stopPropagation()
              onNegate(option)
            }}>
              <BlockIcon style={{color: negated.includes(option) ? 'red' : 'grey'}} />
            </IconButton>
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  );
}
