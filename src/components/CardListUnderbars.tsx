import React from 'react'

import { CardId } from 'src/models'

import Stack from '@mui/material/Stack'
import IconButton from '@mui/material/IconButton'
import AddIcon from '@mui/icons-material/Add'
import RemoveIcon from '@mui/icons-material/Remove'
import CircleOutlinedIcon from '@mui/icons-material/CircleOutlined'
import CircleIcon from '@mui/icons-material/Circle'
import Button from '@mui/material/Button'


export type CardListUnderbar = (id: CardId) => JSX.Element


export function DeckCardsUnderbar(props: {
  addCard: (id: CardId) => void,
  deleteCard: (id: CardId) => void,
  cardCounts: {[key in CardId]?: number}
}): CardListUnderbar {

  const {addCard, deleteCard, cardCounts} = props

  const CountIndicator = (id: CardId, c: number) => {
    const count = cardCounts[id]
    if (count !== undefined && count >= c) {
      return <CircleIcon fontSize='small' color='secondary' />
    } else {
      return <CircleOutlinedIcon fontSize='small' color='primary' />
    }
  }

  const DeckCardsUnderbarInstance = (id: CardId) => (
    <Stack
      direction='row'
      justifyContent='center'
      alignItems='center'
    >
      <IconButton onClick={() => {
        deleteCard(id)
      }}>
        <RemoveIcon fontSize='small' />
      </IconButton>
      {CountIndicator(id, 1)}
      {CountIndicator(id, 2)}
      {CountIndicator(id, 3)}
      {CountIndicator(id, 4)}
      <IconButton onClick={() => {
        addCard(id)
      }}>
        <AddIcon fontSize='small' />
      </IconButton>
    </Stack>
  )

  DeckCardsUnderbarInstance.displayName = 'DeckCardsUnderbarInstance'

  return DeckCardsUnderbarInstance
}

export function SelectCardUnderbar(props: {
  currentSelection?: CardId,
  onSelect: (id: CardId) => void,
  onCancel: () => void
}): CardListUnderbar {

  const {currentSelection, onSelect, onCancel} = props

  const SelectCardUnderbarInstance = (id: CardId) => (
    <Stack
      direction='row'
      justifyContent='center'
      alignItems='center'
    >
      <Button
        onClick={onCancel}
        variant='text'
      >
        Cancel
      </Button>
      <Button
        onClick={() => onSelect(id)}
        variant='contained'
        disabled={id === currentSelection}
      >
        {id === currentSelection ? 'Selected' : 'Select'}
      </Button>
    </Stack>
  )

  SelectCardUnderbarInstance.displayName = 'SelectCardUnderbarInstance'

  return SelectCardUnderbarInstance
}