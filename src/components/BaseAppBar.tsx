import React from 'react'

import AppBar from '@mui/material/AppBar';
import Toolbar from '@mui/material/Toolbar';
import { useTheme } from '@mui/material';

export default function BaseAppBar(props: {
  children: React.ReactNode
}) {
  const {children} = props

  const theme = useTheme()

  return (
    <AppBar
      position="static"
      elevation={4}
      sx={{
        backgroundColor: theme.palette.grey[900],
        borderBottom: (theme) => `1px solid ${theme.palette.divider}`
      }}
    >
      <Toolbar sx={{
        ...theme.mixins.toolbar,
        flexWrap: 'wrap',
        paddingX: theme.spacing()
      }}>
        {children}
      </Toolbar>
    </AppBar>
  )
}