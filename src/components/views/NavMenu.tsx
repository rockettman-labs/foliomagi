import React from 'react';

import { useAuthenticator } from '@aws-amplify/ui-react';

import Image from 'next/image';
import { useRouter } from 'next/router';

import logo from 'public/images/logos/logo192.webp';
import Link from 'src/components/Link';

import Box from '@mui/material/Box';
import List from '@mui/material/List';
import Divider from '@mui/material/Divider';
import SearchIcon from '@mui/icons-material/Search';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import AutoStoriesOutlinedIcon from '@mui/icons-material/AutoStoriesOutlined';
import LoginOutlinedIcon from '@mui/icons-material/LoginOutlined';
import LogoutOutlinedIcon from '@mui/icons-material/LogoutOutlined';
import HomeOutlinedIcon from '@mui/icons-material/HomeOutlined';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemAvatar from '@mui/material/ListItemAvatar';

export default function NavMenu() {

  const { user, signOut, authStatus } = useAuthenticator(context => [context.user, context.authStatus]);
  const { replace, asPath } = useRouter();

  const signOutAndRedirect = () => {
    signOut()
    replace('/')
  }

  return (
    <Box
      role='presentation'
      sx={{marginRight: (theme) => theme.spacing(2)}}
    >
      <List>
        <ListItem>
          <ListItemAvatar sx={{marginRight: (theme) => theme.spacing()}}>
            <Image src={logo} layout='responsive' alt='FolioMagi logo' />
          </ListItemAvatar>
          <ListItemText
            primary='FolioMagi'
            secondary={authStatus === "authenticated" ? user?.username : 'Guest'}
          />
        </ListItem>
        <Divider />
        <Link href='/'>
          <ListItemButton
            selected={asPath === '/'}
            key='home'
          >
            <ListItemIcon>
              <HomeOutlinedIcon />
            </ListItemIcon>
            <ListItemText secondary='Home' />
          </ListItemButton>
        </Link>
        <Link href='/search'>
          <ListItemButton
            selected={asPath === '/search'}
            key='search'
          >
            <ListItemIcon>
              <SearchIcon />
            </ListItemIcon>
            <ListItemText secondary='Search Cards' />
          </ListItemButton>
        </Link>
        <Link href='/decks'>
          <ListItemButton
            selected={asPath === '/decks'}
            disabled={authStatus === "unauthenticated"}
            key='decks'
          >
            <ListItemIcon>
              <AutoStoriesOutlinedIcon />
            </ListItemIcon>
            <ListItemText secondary='Decks' />
          </ListItemButton>
        </Link>
      </List>
      <Divider />
      <List>
        { authStatus === "authenticated" ?
          <ListItemButton onClick={signOutAndRedirect} key='signout'>
            <ListItemIcon>
              <LogoutOutlinedIcon />
            </ListItemIcon>
            <ListItemText secondary='Sign Out' />
          </ListItemButton> :
          <Link href='/signin'>
            <ListItemButton key='signin'>
              <ListItemIcon>
                <LoginOutlinedIcon />
              </ListItemIcon>
              <ListItemText secondary='Sign In' />
            </ListItemButton>
          </Link>
        }
      </List>
    </Box>
  )
}
