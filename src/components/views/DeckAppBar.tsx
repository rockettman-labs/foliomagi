import React from 'react'

import { useRouter } from 'next/router';

import BaseAppBar from 'src/components/BaseAppBar';
import { Deck } from 'src/models';
import useToggle from 'src/hooks/useToggle';

import IconButton from '@mui/material/IconButton';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import Typography from '@mui/material/Typography';
import EditIcon from '@mui/icons-material/Edit';
import CheckIcon from '@mui/icons-material/Check';
import Box from '@mui/system/Box';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';

export default function DeckAppBar(props: {
  deck: Deck,
  unsavedChanges: boolean,
  onSaveDeckName: (newDeckName: string) => void,
  onSave: () => void
}) {
  const {deck, unsavedChanges, onSaveDeckName, onSave} = props

  const router = useRouter()

  const [deckName, setDeckName] = React.useState<string>('')

  const [
    editingDeckName,
    startEditingDeckName,
    stopEditingDeckName
  ] = useToggle()

  const onStopEditingDeckName = () => {
    stopEditingDeckName()
    onSaveDeckName(deckName)
  }

  React.useEffect(() => {
    setDeckName(deck.name)
  }, [deck])

  return (
    <BaseAppBar>
      <IconButton
        aria-label="back"
        onClick={router.back}
      >
        <ArrowBackIcon />
      </IconButton>
      {editingDeckName ?
        <TextField
          value={deckName}
          onChange={event => setDeckName(event.target.value)}
          size='small'
          onFocus={event => event.target.select()}
        />
        :
        <Typography variant='h6'>{deckName}</Typography>
      }
      {editingDeckName ?
        <IconButton onClick={onStopEditingDeckName}>
          <CheckIcon />
        </IconButton>
        :
        <IconButton onClick={startEditingDeckName}>
          <EditIcon />
        </IconButton>
      }
      <Box flexGrow={1} />
      {unsavedChanges ?
        <Button
          color='secondary'
          variant='contained'
          onClick={onSave}
        >
          Save
        </Button>
        :
        <Button variant='text'>
          Saved
        </Button>
      }
    </BaseAppBar>
  )
}