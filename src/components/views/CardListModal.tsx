import React from 'react'

import SquareCardImage from 'src/components/SquareCardImage'
import { CardId } from 'src/models'
import { Card } from 'src/cards'
import { CardListUnderbar } from 'src/components/CardListUnderbars'

import Dialog from '@mui/material/Dialog'
import DialogContent from '@mui/material/DialogContent'
import DialogActions from '@mui/material/DialogActions'
import Stack from '@mui/material/Stack'
import Box from '@mui/material/Box'
import IconButton from '@mui/material/IconButton'
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft'
import ChevronRightIcon from '@mui/icons-material/ChevronRight'
import DialogTitle from '@mui/material/DialogTitle'

export default function CardListModal(props: {
  cards: Card[],
  initialCardShown?: CardId,
  open: boolean,
  actions?: CardListUnderbar,
  onClose: () => void
}) {
  const {cards, initialCardShown, open, actions, onClose} = props

  const [currentIndex, setCurrentIndex] = React.useState(0)

  React.useEffect(() => {
    const initialCardIndex = cards.findIndex(c => c.id === initialCardShown)
    setCurrentIndex(initialCardIndex != -1 ? initialCardIndex : 0)
  }, [cards, initialCardShown])

  const goToPreviousCard = () => {
    if (currentIndex > 0) {
      setCurrentIndex(currentIndex - 1)
    } else {
      setCurrentIndex(cards.length - 1)
    }
  }

  const goToNextCard = () => {
    if (currentIndex < (cards.length - 1) ) {
      setCurrentIndex(currentIndex + 1)
    } else {
      setCurrentIndex(0)
    }
  }

  return (
    <Dialog
      open={open}
      onClose={onClose}
      PaperProps={{
        sx: {
          flex: 1,
          backgroundColor: 'transparent',
          backgroundImage: 'url(/images/vertical-parchment.webp)',
          backgroundSize: '100% 100%'
        }
      }}
    >
      <DialogTitle
        color='black'
        sx={{
          textAlign: 'center'
        }}
      >
        {`${currentIndex + 1}/${cards.length}`}
      </DialogTitle>
      <DialogContent>
        <Stack direction='row' alignItems='center'>
          <Box sx={{
            flex: 'none',
            marginRight: (theme) => theme.spacing(2)
          }}>
            <IconButton onClick={goToPreviousCard}>
              <ChevronLeftIcon fontSize='large' htmlColor='#000000' />
            </IconButton>
          </Box>
          <Box sx={{
            display: 'flex',
            flex: 1
          }}>
            <Box sx={{
              flex: 1
            }}>
              {cards[currentIndex] &&
                <SquareCardImage card={cards[currentIndex]} />
              }
            </Box>
          </Box>
          <Box sx={{
            flex: 'none',
            marginLeft: (theme) => theme.spacing(2)
          }}>
            <IconButton onClick={goToNextCard}>
              <ChevronRightIcon fontSize='large' htmlColor='#000000' />
            </IconButton>
          </Box>
        </Stack>
      </DialogContent>
      {actions && cards[currentIndex] &&
        <DialogActions sx={{ justifyContent: 'center' }}>
            {actions(cards[currentIndex].id as CardId)}
        </DialogActions>
      }
    </Dialog>
  )
}
