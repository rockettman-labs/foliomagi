import React from 'react';

import { Card } from 'src/cards';
import SquareCardImage from 'src/components/SquareCardImage';
import { CardListUnderbar } from 'src/components/CardListUnderbars';
import { CardId } from 'src/models';

import Grid from '@mui/material/Grid';


export default function CardList(props: {
  cards: Card[],
  underbar?: CardListUnderbar,
  onCardClick?: (cardId: CardId) => void,
}) {
  const {cards, underbar, onCardClick} = props

  const getCardClickCallback = (cardId: CardId) => {
    if (onCardClick !== undefined) {
      return () => onCardClick(cardId)
    } else {
      return undefined
    }
  }

  return (
    <Grid container spacing={2}>
      {cards.map(card => {
        return (
          <Grid item key={card.id} xs={6} md={4} lg={3} xl={2}>
            <SquareCardImage card={card} onClick={getCardClickCallback(card.id as CardId)} />
            { underbar ? underbar(card.id as CardId) : undefined }
          </Grid>
        )
      })}
    </Grid>
  )
}