import React from 'react';

import BaseAppBar from 'src/components/BaseAppBar';
import NavMenu from 'src/components/views/NavMenu';
import useToggle from 'src/hooks/useToggle';

import { useTheme } from '@mui/material/styles';
import Typography from '@mui/material/Typography';
import MenuIcon from '@mui/icons-material/Menu';
import IconButton from '@mui/material/IconButton';
import Drawer from '@mui/material/Drawer';


export default function MainAppBar(props: {
  title?: string,
}) {

  const theme = useTheme();

  const [
    navDrawerIsOpen,
    openNavDrawer,
    closeNavDrawer
  ] = useToggle()

  return (
    <React.Fragment>
      <BaseAppBar>
        <IconButton
          aria-label="menu"
          onClick={openNavDrawer}
        >
          <MenuIcon />
        </IconButton>
        { props.title &&
          <Typography variant="h6" noWrap>
            {props.title}
          </Typography>
        }
      </BaseAppBar>
      <Drawer
        anchor='left'
        open={navDrawerIsOpen}
        onClose={closeNavDrawer}
      >
        <NavMenu />
      </Drawer>
    </React.Fragment>
  );
};