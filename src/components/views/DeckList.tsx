import React from 'react'

import { Deck } from 'src/models'
import DeckListItem from 'src/components/views/DeckListItem'

import Grid from '@mui/material/Grid'

export default function DeckList(props: {
  decks: Deck[]
}) {
  const {decks} = props

  return (
    <Grid container spacing={1}>
      {decks.map(deck => (
        <Grid item key={deck.id} xs={6} sm={4} md={3} lg={2} xl={1}>
          <DeckListItem deck={deck} />
        </Grid>
      ))}
    </Grid>
  )
}