import * as React from 'react';

import { NextPage } from 'next';
import Image from 'next/image';

import styles from 'src/styles/HomePage.module.css';
import logo from 'public/images/logos/logo192-greyoutlined.webp';
import { useAuthenticator } from '@aws-amplify/ui-react-core';
import MainAppBar from 'src/components/views/MainAppBar';

import Link from '@mui/material/Link';
import Button from '@mui/material/Button';
import Box from '@mui/material/Box';
import Typography from '@mui/material/Typography';

const HomePage: NextPage = () => {
  const { authStatus } = useAuthenticator(context => [context.authStatus]);

  const signInButton = (
    <Link href='/signin'>
      <Button color='primary' variant='contained'>
        Sign In
      </Button>
    </Link>
  )

  return (
    <React.Fragment>
      <MainAppBar />
      <Box>
        <header className={styles.AppHeader}>
          <Image src={logo} className={styles.AppLogo} alt="logo" />
          <Typography variant='h4'>
            FolioMagi
          </Typography>
          <Typography>
            Search cards, build decks, have fun!
          </Typography>
          { authStatus === "unauthenticated" &&
            <Box sx={{marginTop: (theme) => theme.spacing(4)}}>
              {signInButton}
            </Box>
          }
        </header>
      </Box>
    </React.Fragment>
  );
}

export default HomePage;
