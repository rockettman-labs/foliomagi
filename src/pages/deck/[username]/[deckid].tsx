import * as React from 'react';

import { NextPage } from 'next';
import { useRouter } from 'next/router'

import { DataStore } from 'aws-amplify/datastore';
import { MutableModel } from '@aws-amplify/datastore';

import { decreaseCardCount, increaseCardCount, newDeck } from 'src/utils';
import TopLevelPageContiner from 'src/components/TopLevelPageContainer';
import ToolbarLayout from 'src/components/layouts/ToolbarLayout';
import DeckAppBar from 'src/components/views/DeckAppBar';
import AdaptiveDrawerLayout from 'src/components/layouts/AdaptiveDrawerLayout';
import TabLayout from 'src/components/layouts/TabLayout';
import DeckView from 'src/components/views/DeckView';
import CardSearch from 'src/components/views/CardSearch';
import { CardId, Deck, DeckMetaData } from 'src/models';
import { DeckCardsUnderbar } from 'src/components/CardListUnderbars';
import useToggle from 'src/hooks/useToggle';

import Box from '@mui/material/Box';
import { useMediaQuery, useTheme } from '@mui/material';
import ArrowLeftIcon from '@mui/icons-material/ArrowLeft';


export type ActiveCardList = 'main' | 'sideboard'

const DecksPage: NextPage = () => {

  const router = useRouter()
  const {username, deckid} = router.query

  const theme = useTheme()
  const isWidescreen = useMediaQuery(theme.breakpoints.up('md'))

  const [deck, setDeck] = React.useState<Deck>(newDeck())
  const [
    unsavedChanges,
    setUnsavedChanges,
    setChangesSaved
  ] = useToggle()
  const [activeCardList, setActiveCardList] = React.useState<ActiveCardList>('main')

  React.useEffect(() => {

    async function loadDeck(id: string) {
      const thisDeck = await DataStore.query(Deck, id)
      if (thisDeck !== undefined) {
        setDeck(thisDeck)
      }
    }

    if (deckid !== undefined) {
      loadDeck(deckid as string)
    }
  }, [deckid])

  const cardCounts = React.useMemo(() => {
    let counts: {[key in CardId]?: number} = {}
    deck.cards.concat(deck.sideboard).forEach(cardCount => {
      const id = CardId[cardCount.card as keyof typeof CardId]
      counts[id] = counts[id] ? counts[id]! + cardCount.count : cardCount.count
    })
    return counts
  }, [deck])

  async function saveDeck() {
    const current = await DataStore.query(Deck, deck.id)

    if (current) {
      const updatedDeck = await DataStore.save(
        Deck.copyOf(current, updated => {
          updated.name = deck.name
          updated.startingCharacter = deck.startingCharacter
          updated.lessons = deck.lessons
          updated.cards = deck.cards
          updated.sideboard = deck.sideboard
        })
      )
      setDeck(updatedDeck)
      setChangesSaved()
    }
  }

  function modifyDeck(mutator: (draft: MutableModel<Deck, DeckMetaData>) => void | MutableModel<Deck, DeckMetaData>) {
    setDeck(Deck.copyOf(deck, mutator))
    setUnsavedChanges()
  }

  const DeckTabs = [
    {
      label: 'Deck',
      contents: (
        <DeckView
          deck={deck}
          onChangeStartingCharacter={id => {
            modifyDeck(updated => {
              updated.startingCharacter = id
            })
          }}
          onSetLessonCounts={counts => {
            modifyDeck(updated => {
              updated.lessons = counts
            })
          }}
          onDeleteCard={id => {
            // TODO
            console.log(`Delete ${id} from card list`)
          }}
          onDeleteSideboardCard={id => {
            // TODO
            console.log(`Delete ${id} from sideboard`)
          }}
          setActiveCardList={setActiveCardList}
        />
      )
    }
  ]

  const DeckCardSearch = (
    <CardSearch
      cardListUnderbar={
        DeckCardsUnderbar({
          addCard: (id) => {
            const currentCount = cardCounts[id]
            if (currentCount === undefined || currentCount < 4) {
              if (activeCardList === 'main') {
                modifyDeck(updated => {
                  updated.cards = increaseCardCount(deck.cards, id)
                })
              } else {
                modifyDeck(updated => {
                  updated.sideboard = increaseCardCount(deck.sideboard, id)
                })
              }
            }
          },
          deleteCard: (id) => {
            if (activeCardList === 'main') {
              modifyDeck(updated => {
                updated.cards = decreaseCardCount(deck.cards, id)
              })
            } else {
              modifyDeck(updated => {
                updated.sideboard= decreaseCardCount(deck.sideboard, id)
              })
            }
          },
          cardCounts: cardCounts
        })
      }
    />
  )

  return (
    <TopLevelPageContiner id='page-container'>
      <ToolbarLayout
        label='deck-page'
        toolbar={
          <DeckAppBar
            deck={deck}
            unsavedChanges={unsavedChanges}
            onSaveDeckName={deckName => {
              modifyDeck(updated => {
                updated.name = deckName
              })
            }}
            onSave={saveDeck}
          />
        }
      >
        {isWidescreen ?
          <AdaptiveDrawerLayout
            label='deck-page-layout'
            anchor='left'
            open={true}
            drawerContents={
              <Box sx={{
                display: 'flex',
                flex: 1,
                height: '100%'
              }}>
                <Box sx={{ flex: 1 }}>
                  <TabLayout
                    label='deck-tabs'
                    tabs={DeckTabs}
                  />
                </Box>
                <Box sx={{
                  display: 'flex',
                  alignItems: 'center',
                  justifyContent: 'center',
                  flex: 'none',
                  width: '5%',
                  minWidth: '20px',
                  backgroundColor: '#00000030',
                  '&:hover': {
                    backgroundColor: '#00000040'
                  }
                }}
                >
                  <ArrowLeftIcon fontSize='small' />
                </Box>
              </Box>
            }
          >
            {DeckCardSearch}
          </AdaptiveDrawerLayout>
          :
          <TabLayout
            label='deck-tabs'
            tabs={[
              ...DeckTabs,
              {
                label: 'Add Cards',
                contents: DeckCardSearch
              },
            ]}
          />
        }
      </ToolbarLayout>
    </TopLevelPageContiner>
  );
}

export default DecksPage;
