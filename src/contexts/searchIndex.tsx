import React from 'react';

import { CardCatalog } from 'src/cards';


type SearchIndexState = {
  cardCatalog?: CardCatalog,
}

export const SearchIndexContext = React.createContext<SearchIndexState>({})

export const SearchIndexProvider: React.FC<React.PropsWithChildren> = ({children}) => {
  const [indexes, setIndexes] = React.useState<SearchIndexState>({});

  React.useEffect(() => {
    initCardCatalog()
  }, []);

  function initCardCatalog() {
    const cardCatalog = new CardCatalog()
    cardCatalog.hydrate()
      .then(() => {
        setIndexes({
          cardCatalog: cardCatalog,
        })
      })
      .catch((e) => {
        console.error('[ERROR] Failed to hydrate card catalog')
        console.error(JSON.stringify(e, null, 2))
      })
  }

  return (
    <SearchIndexContext.Provider value={indexes}>
      {children}
    </SearchIndexContext.Provider>
  )
}
