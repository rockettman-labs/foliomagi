import { createTheme, responsiveFontSizes } from '@mui/material/styles';

// Create a theme instance.
const theme = responsiveFontSizes(createTheme({
  palette: {
    mode: 'dark',
    primary: {
      main: '#69319c',
    },
    secondary: {
      main: '#e79e32',
    }
  },
}))

export default theme;
