import React from 'react'

type ToggleFunction = () => void

/**
 * Hook to manage on/off, open/close state
 */
export default function useToggle(startsOn = false): [boolean, ToggleFunction, ToggleFunction] {

  const [isOn, setOn] = React.useState(startsOn)

  const on = () => setOn(true)
  const off = () => setOn(false)

  return [isOn, on, off]
}