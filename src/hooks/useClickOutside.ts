import React from 'react'

/**
 * Hook that calls function on mousedown events outside of the passed ref
 */
export default function useClickOutside(ref: React.RefObject<any>, callback: () => void) {
  React.useEffect(() => {
      /**
       * Alert if clicked on outside of element
       */
      function handleClickOutside(event: MouseEvent) {
          if (ref.current && !ref.current.contains(event.target)) {
            callback()
          }
      }

      // Bind the event listener
      document.addEventListener("mousedown", handleClickOutside);
      return () => {
          // Unbind the event listener on clean up
          document.removeEventListener("mousedown", handleClickOutside);
      };
  }, [ref, callback]);
}
