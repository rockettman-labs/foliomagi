
export function isNumeric(s: string) {
  return !isNaN(Number(s));
}

export function timeIt(func: Function) {
  const start = Date.now();
  const result = func();
  const end = Date.now();
  return {
    funcResponse: result,
    executionMillis: end - start
  }
}

export async function asyncTimeIt(func: Function) {
  const start = Date.now();
  const result = await func();
  const end = Date.now();
  return {
    funcResponse: result,
    executionMillis: end - start
  }
}