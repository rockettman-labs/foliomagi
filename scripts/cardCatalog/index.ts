
import * as path from 'path';
import updateCardCatalog from './updateCardCatalog';
import viewCardCatalog from './viewCardCatalog';
import generateGraphqlCardIdEnum from './generateGraphqlCardIdEnum';
import searchCatalog from './searchCardCatalog';

const CARD_CATALOG_FILE = path.join(__dirname, '..', '..', 'src', 'cards', 'cardSets.ts');
const CARD_IDS_ENUM_FILE = path.join(__dirname, '..', '..', 'amplify', 'backend', 'api', 'FolioMagiApi', 'schema', 'cardIds.graphql')

const commandLineArgs = process.argv;

if (commandLineArgs.includes('updateCatalog')) {
  updateCardCatalog(CARD_CATALOG_FILE);
} else if (commandLineArgs.includes('view')) {
  viewCardCatalog().then(() => {process.exit(0)})
} else if (commandLineArgs.includes('updateGraphqlEnum')) {
  generateGraphqlCardIdEnum(CARD_IDS_ENUM_FILE);
} else if (commandLineArgs.includes('search')) {
  const query = commandLineArgs[commandLineArgs.length - 1]
  searchCatalog(query).then(() => {process.exit(0)})
}
