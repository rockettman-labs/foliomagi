
import { Card, CardType, LessonType, HPTCGSet, HPTCGSetName, cardSets } from 'src/cards';


export default async function viewCardCatalog() {
  console.log('Printing catalog...')
  await printCardCatalog(cardSets);
  console.log('Done')
  console.log()
  //printSummary(cardCatalog);
}

async function printCardCatalog(cardCatalog: HPTCGSet[]) {
  for await (const cardSet of cardCatalog) {
    for await (const card of cardSet.cards) {
      console.log(card);
      await keypress();
    }
  }
}

function printSummary(cards: Card[]) {
  const typeCounts: Map<CardType, number> = new Map();
  const lessonCounts: Map<LessonType, number> = new Map();
  const setTypeCounts: Map<HPTCGSetName, Map<CardType, number>> = new Map();

  cards.forEach(card => {
    const typeCount = typeCounts.has(card.type) ? typeCounts.get(card.type) : 0
    typeCounts.set(card.type, typeCount! + 1)

    if (card.powerCost) {
      const lessonCount = lessonCounts.has(card.powerCost.type) ? lessonCounts.get(card.powerCost.type): 0
      lessonCounts.set(card.powerCost.type, lessonCount! + 1)
    }

    let setCounts = setTypeCounts.get(card.set)
    if (!setCounts) {
      setCounts = new Map()
      setTypeCounts.set(card.set, setCounts)
    }
    const setTypeCount = setCounts.has(card.type) ? setCounts.get(card.type) : 0
    setCounts.set(card.type, setTypeCount! + 1)
  })

  console.log('***')
  console.log('SUMMARY:')
  console.log('***')
  Object.keys(CardType).forEach(cardType => {
    console.log(`- ${cardType}: ${typeCounts.get(cardType as CardType)}`)
  })
  console.log('***')
  Object.keys(LessonType).forEach(lessonType => {
    console.log(`- ${lessonType}: ${lessonCounts.get(lessonType as LessonType)}`)
  })
  console.log('***')
  console.log(`TOTAL cards: ${cards.length}/${116+80+80+80+140}`)
  console.log()
  console.log('*** Set Level ***')
  Object.keys(HPTCGSetName).forEach(set => {
    console.log(`${set}:`)
    const setCounts = setTypeCounts.get(set as HPTCGSetName)
    let totalSetCards = 0
    Object.keys(CardType).forEach(cardType => {
      const typeCount = setCounts!.has(cardType as CardType) ? setCounts!.get(cardType as CardType) : 0
      totalSetCards += typeCount!
      console.log(`- ${cardType}: ${typeCount}`)
    })
    console.log(`TOTAL: ${totalSetCards}`)
  })
}

// Use 'await keypress()' to wait for any key press to continue
async function keypress() {
  process.stdin.setRawMode(true);
  return new Promise<void>(resolve => process.stdin.once('data', data => {
    const byteArray = [...data];
    if (byteArray.length > 0 && byteArray[0] === 3) {
      console.log('^C');
      process.exit(1);
    }
    process.stdin.setRawMode(false);
    resolve();
  }))
}