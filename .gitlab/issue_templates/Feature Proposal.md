## Feature Proposal

(Summarize your proposal concisely)

## Description

(If necessary, go into greater detail about this change)

(Feel free to add screenshots, mockups, inspiration from other applications, you name it)

## Why is this change needed? How would this improve the user experience?

(Has to be improving something for someone to be worth doing)

## Implementation ideas

(If possible, suggest how to implement your feature: suggested tools/libraries, design patterns, etc)


/label ~"type::discussion" ~"status::triage"
/cc @rockettman
