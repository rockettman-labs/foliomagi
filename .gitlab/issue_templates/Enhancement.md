## Enhancement

(Describe the change concisely)

## From Feature Proposal

(Add link to feature proposal if there was one)

## Implementation ideas

(If possible, suggest how to implement this change: suggested tools/libraries, design patterns, relevant files, etc)


/label ~"type::enhancement" ~"status::triage"
/cc @rockettman
