## What needs to be documented?

(What)

## Where should it be documented?

(Where)

## Are the current docs innacurate or missing this information?
[ ] Innacurate
[ ] Missing


/label ~"type::documentation" ~"status::triage"
/cc @rockettman
